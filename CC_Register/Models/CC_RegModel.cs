﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CC_Register.Models
{
    public class CC_RegModel
    {
        public int RegId { get; set; }
        public string  DistrictCode { get; set; }
        public string BlockCode { get; set; }

    }
}
